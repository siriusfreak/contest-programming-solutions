#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <string>
#include <cstring>
#include <map>
#include <vector>
#include <deque>

using namespace std;
int ans[305][3005],n,a[305],i,j;

bool dfs(int x,int y,int z) {
    if (x == 0) return z;
    for (int j = (z ? (x == ans[i][0] ? 1 : 0) : ans[i - 1][x]); j <= 9; j++)
        if (j + 9 * (x - 1) >= y && y >= j)
            if (dfs(x - 1, y - j, z || j > ans[i - 1][x])) {
                ans[i][x] = j;
                return true;
            }
    return false;
}

int main() {
    scanf("%d", &n);
    for (i = 1; i <= n; i++)
        scanf("%d", &a[i]);

    while (a[1] > 9) {
        a[1] -= 9;
        ans[1][++ans[1][0]] = 9;
    }
    ans[1][++ans[1][0]] = a[1];

    for (j = ans[1][0]; j >= 1; j--) cout << ans[1][j];
    cout << endl;
    for (i = 2; i <= n; i++) {
        ans[i][0] = ans[i - 1][0];
        while (ans[i][0] * 9 < a[i]) ans[i][0]++;
        if (!(dfs(ans[i][0], a[i], ans[i][0] > ans[i - 1][0]))) {
            ans[i][0]++;
            dfs(ans[i][0], a[i], 1);
        }
        for (j = ans[i][0]; j >= 1; j--) printf("%d", ans[i][j]);
        printf("\n");
    }
    return 0;
}